# Sokolov Aleksandr Test Task

* Основной compose файл `docker-compose.yml

```
cd existing_repo
git remote add origin https://gitlab.com/assoed2/lamoda_test_task
git branch -M main
git push -uf origin main
```

## Запуск

```bash
docker compose up -d 
````

или

```bash
make up
````

## Установка зависимостей composer

```bash
make composer run="install" 
````

или

```bash
docker-compose exec php-fpm composer install
````

## Миграции

```bash[.gitignore](.gitignore)
docker compose exec php php bin/console doctrine:migrations:migrate -n

```

или

```bash
make composer run="doctrine:migrations:migrate -n"
```

## Документация Swagger по адресу localhost/api/doc