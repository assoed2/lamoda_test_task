<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240604140001 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE inventory DROP CONSTRAINT fk_b12d4a365080ecde');
        $this->addSql('DROP INDEX idx_b12d4a365080ecde');
        $this->addSql('ALTER TABLE inventory RENAME COLUMN warehouse_id TO storage_id');
        $this->addSql('ALTER TABLE inventory ADD CONSTRAINT FK_B12D4A365CC5DB90 FOREIGN KEY (storage_id) REFERENCES storage (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_B12D4A365CC5DB90 ON inventory (storage_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE inventory DROP CONSTRAINT FK_B12D4A365CC5DB90');
        $this->addSql('DROP INDEX IDX_B12D4A365CC5DB90');
        $this->addSql('ALTER TABLE inventory RENAME COLUMN storage_id TO warehouse_id');
        $this->addSql('ALTER TABLE inventory ADD CONSTRAINT fk_b12d4a365080ecde FOREIGN KEY (warehouse_id) REFERENCES storage (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_b12d4a365080ecde ON inventory (warehouse_id)');
    }
}
