# Variables
DOCKER_COMPOSE = sudo docker-compose

up: ## Поднять проект.
	@$(DOCKER_COMPOSE) up -d

down: ## Остановить проект.
	@$(DOCKER_COMPOSE) down --remove-orphans

composer: ## Взаимодействие с composer. make composer run="install"
	@$(DOCKER_COMPOSE) exec php-fpm composer $(run)

command: ## Запустить консольную команду проекта. make command run="doctrine:migrations:migrate -n --no-debug"
	@$(DOCKER_COMPOSE)  exec php-fpm php ./bin/console $(run)


rebuild: ## Пересобрать все образы и запустить проект.
	@$(DOCKER_COMPOSE) build --no-cache
	@$(DOCKER_COMPOSE) up -d

