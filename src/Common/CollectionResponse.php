<?php

namespace App\Common;

class CollectionResponse implements QueryResponse
{
    protected array $items;

    public function __construct(
        array $items,
    ) {
        $this->items = $items;
    }

    public function withResult($result): self
    {
        $new = clone $this;

        \array_unshift($new->items, $result);

        return $new;
    }

    public function result()
    {
        return \reset($this->items);
    }

    public function results(): array
    {
        return $this->items;
    }
}
