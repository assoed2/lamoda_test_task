<?php

declare(strict_types=1);

namespace App\Common;

interface CommandBus
{
    public function execute(Command $command): void;
}
