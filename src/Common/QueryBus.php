<?php

declare(strict_types=1);

namespace App\Common;

use Doctrine\Common\Collections\ArrayCollection;

interface QueryBus
{
    public function query(Query $query): ArrayCollection;
}
