<?php

namespace App\DTO;

class InventoryDTO
{
    private int $id;
    private ProductDTO $product;
    private StorageDTO $storage;
    private int $amount;

    public function __construct(int $id, ProductDTO $product, StorageDTO $storage, int $amount)
    {
        $this->id = $id;
        $this->product = $product;
        $this->storage = $storage;
        $this->amount = $amount;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getProduct(): ProductDTO
    {
        return $this->product;
    }

    public function getStorage(): StorageDTO
    {
        return $this->storage;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): void
    {
        $this->amount = $amount;
    }

    public function setStorage(StorageDTO $storage): void
    {
        $this->storage = $storage;
    }

    public function setProduct(ProductDTO $product): void
    {
        $this->product = $product;
    }
}
