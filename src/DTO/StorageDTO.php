<?php

namespace App\DTO;

use App\Entity\Storage;

class StorageDTO
{
    private int $id;
    private string $name;
    private string $status;

    public function __construct(int $id, string $name, string $status)
    {
        $this->id = $id;
        $this->name = $name;
        $this->status = $status;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public static function fromObject(object $object): self
    {
        if (!$object instanceof Storage) {
            throw new \TypeError();
        }

        return new self(
            id: $object->getId(),
            name: $object->getName(),
            status: $object->getStatus()
        );
    }
}
