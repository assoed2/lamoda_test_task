<?php

namespace App\DTO;

use App\Entity\Product;

class ProductDTO
{
    private int $id;
    private string $name;
    private string $uniqueCode;
    private string $size;

    public function __construct(int $id, string $name, string $uniqueCode, string $size)
    {
        $this->id = $id;
        $this->name = $name;
        $this->uniqueCode = $uniqueCode;
        $this->size = $size;
    }

    public static function fromObject(object $object): self
    {
        if (!$object instanceof Product) {
            throw new \TypeError();
        }

        return new self(
            id: $object->getId(),
            name: $object->getName(),
            uniqueCode: $object->getUniqueCode(),
            size: $object->getSize(),
        );
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getUniqueCode(): string
    {
        return $this->uniqueCode;
    }

    public function setUniqueCode(string $uniqueCode): void
    {
        $this->uniqueCode = $uniqueCode;
    }

    public function getSize(): string
    {
        return $this->size;
    }

    public function setSize(string $size): void
    {
        $this->size = $size;
    }
}
