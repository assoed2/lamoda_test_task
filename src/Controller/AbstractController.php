<?php

namespace App\Controller;

use App\Common\Command;
use App\Common\CommandBus;
use App\Common\Query;
use App\Common\QueryBus;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractController implements SerializerAwareInterface
{
    private CommandBus $commandBus;
    protected Request $request;
    private QueryBus $queryBus;
    protected SerializerInterface $serializer;

    public function __construct(
        CommandBus $commandBus,
        RequestStack $requestStack,
        QueryBus $queryBus,
        SerializerInterface $serializer,
    ) {
        $this->commandBus = $commandBus;
        $this->request = $requestStack->getCurrentRequest();
        $this->queryBus = $queryBus;
        $this->serializer = $serializer;
    }

    protected function execute(Command $command): void
    {
        $this->commandBus->execute($command);
    }

    protected function getRequest(): Request
    {
        return $this->request;
    }

    protected function query(Query $query): ArrayCollection
    {
        return $this->queryBus->query($query);
    }

    protected function json($data, int $status = 200, array $headers = [], array $context = []): JsonResponse
    {
        $json = $this->serializer->serialize(
            $data,
            'json',
            \array_merge(['json_encode_options' => JsonResponse::DEFAULT_ENCODING_OPTIONS], $context)
        );

        return new JsonResponse($json, $status, $headers, true);
    }

    public function setSerializer(SerializerInterface $serializer): void
    {
        $this->serializer = $serializer;
    }
}
