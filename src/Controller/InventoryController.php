<?php

namespace App\Controller;

use App\Action\Inventory\CreateInventory;
use App\Action\Inventory\FreeInventoryReserve;
use App\Action\Inventory\GetInventoryByStorageList;
use App\Action\Inventory\ReserveInventory;
use App\Action\Inventory\UpdateInventory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;

class InventoryController extends AbstractController
{
    /**
     * @OA\Post(summary="Создание остатка на складе")
     *
     * @OA\Response(
     *     response=201
     *     description="Остаток создан"
     * )
     *
     * @throws \Exception
     */
    #[Route('/inventories/create', name: 'app_inventory.create', methods: ['POST'])]
    public function createInventory(): JsonResponse
    {
        $request = json_decode($this->getRequest()->getContent());

        $this->execute(new CreateInventory(
            productId: $request->productId,
            storageId: $request->storageId,
            amount: $request->amount
        )
        );

        return $this->json(null, 201);
    }

    /**
     * @OA\PATCH(summary="Изменение остатка на складе")
     *
     * @OA\Response(
     *     response=204
     *     description="Остаток изменен"
     * )
     *
     * @throws \Exception
     */
    #[Route('/inventories/update', name: 'app_inventory.update', methods: ['PATCH'])]
    public function updateInventory(): JsonResponse
    {
        $request = json_decode($this->getRequest()->getContent());

        $this->execute(new UpdateInventory(
            inventoryId: $request->inventoryId,
            productId: $request->productId,
            storageId: $request->storageId,
            amount: $request->amount
        )
        );

        return $this->json(null, 204);
    }

    /**
     * @OA\GET(summary="Получение списка остатков на складе по Id склада")
     *
     * @OA\Response(
     *     response=200
     *     description="Получение списка остатков на складе по Id склада"
     * )
     *
     * @throws \Exception
     */
    #[Route('/inventories/storages/{id}', name: 'app_inventory.getInventoriesByStorage', methods: ['GET'])]
    public function getInventoriesByStorage($id): JsonResponse
    {
        $result = $this->query(new GetInventoryByStorageList(id: $id)
        );

        return $this->json($result);
    }

    /**
     * @OA\Post (summary="Резервация продукта")
     *
     * @OA\Response(
     *     response=200
     *     description="Резервация продукта на складе"
     * )
     *
     * @throws \Exception
     */
    #[Route('/inventories/reserve', name: 'app_inventory.reserveInventory', methods: ['POST'])]
    public function reserveInventory(): JsonResponse
    {
        $reservationInventories = json_decode($this->getRequest()->getContent(), true)['products'];

        $this->execute(new ReserveInventory($reservationInventories)
        );

        return $this->json(204);
    }

    /**
     * @OA\Post (summary="Резервация продукта")
     *
     * @OA\Response(
     *     response=200
     *     description="Резервация продукта на складе"
     * )
     *
     * @throws \Exception
     */
    #[Route('/inventories/reserve/free', name: 'app_inventory.freeReserveInventory', methods: ['POST'])]
    public function freeReserveInventory(): JsonResponse
    {
        $reservationInventories = json_decode($this->getRequest()->getContent(), true)['products'];
        $this->execute(new FreeInventoryReserve($reservationInventories)
        );

        return $this->json(null, 204);
    }
}
