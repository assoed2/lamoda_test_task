<?php

namespace App\Controller;

use App\Action\Storage\CreateStorage;
use App\Action\Storage\GetStorage;
use App\Action\Storage\GetStorageList;
use App\Action\Storage\UpdateStorage;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;

class StorageController extends AbstractController
{
    /**
     * @OA\Post(summary="Создание склада")
     *
     * @OA\Response(
     *     response=204
     *     description="Склад создан"
     * )
     *
     * @throws \Exception
     */
    #[Route('/storages/create', name: 'app_storage.create', methods: ['POST'])]
    public function createProduct(): JsonResponse
    {
        $request = json_decode($this->getRequest()->getContent());

        $this->execute(new CreateStorage(
            name: $request->name,
            status: $request->status,
        )
        );

        return $this->json(null, 201);
    }

    /**
     * @OA\GET(summary="Получение всех складов")
     *
     * @OA\Response(
     *     response=200
     *     description="Список всех складов"
     * )
     *
     * @throws \Exception
     */
    #[Route('/storages', name: 'app_storage.get', methods: ['GET'])]
    public function getStorages(): JsonResponse
    {
        $results = $this->query(new GetStorageList());

        return $this->json($results);
    }

    /**
     * @OA\PATCH(summary="Обновить склад")
     *
     * @OA\Response(
     *     response=204
     *     description="Склад изменен"
     * )
     *
     * @throws \Exception
     */
    #[Route('/storages/update', name: 'app_storage.update', methods: ['PATCH'])]
    public function updateStorage(): JsonResponse
    {
        $request = json_decode($this->getRequest()->getContent());

        $this->execute(new UpdateStorage(
            id: $request->id,
            name: $request->name,
            status: $request->status,
        )
        );

        return $this->json(null, 204);
    }

    /**
     * @OA\GET(summary="Получение одного склада по id")
     *
     * @OA\Response(
     *     response=200
     *     description="Список всех продуктов"
     * )
     *
     * @throws \Exception
     */
    #[Route('/storages/storage', name: 'app_storage.getStorage', methods: ['GET'])]
    public function getStorage(): JsonResponse
    {
        $request = json_decode($this->getRequest()->getContent());
        $result = $this->query(new GetStorage(id: $request->id));

        return $this->json($result);
    }
}
