<?php

namespace App\Controller;

use App\Action\Product\CreateProduct;
use App\Action\Product\GetProduct;
use App\Action\Product\GetProductsList;
use App\Action\Product\UpdateProduct;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;

class ProductController extends AbstractController
{
    /**
     * @OA\Post(summary="Создание продукта")
     *
     * @OA\Response(
     *     response=201
     *     description="Продукт создан"
     * )
     *
     * @throws \Exception
     */
    #[Route('/products/create', name: 'app_product.create', methods: ['POST'])]
    public function createProduct(): JsonResponse
    {
        $this->execute(new CreateProduct(
            name: $this->getRequest()->get('name'),
            uniqueCode: $this->getRequest()->get('uniqueCode'),
            size: $this->getRequest()->get('size')
        )
        );

        return $this->json(null, 201);
    }

    /**
     * @OA\GET(summary="Получение всех продуктов")
     *
     * @OA\Response(
     *     response=200
     *     description="Список всех продуктов"
     * )
     *
     * @throws \Exception
     */
    #[Route('/products', name: 'app_product.get', methods: ['GET'])]
    public function getProducts(): JsonResponse
    {
        $results = $this->query(new GetProductsList());

        return $this->json($results);
    }

    /**
     * @OA\Put(summary="Обновить продукт")
     *
     * @OA\Response(
     *     response=204
     *     description="Продукт изменен"
     * )
     *
     * @throws \Exception
     */
    #[Route('/products/{id}/update', name: 'app_product.update', methods: ['POST'])]
    public function updateProduct($id): JsonResponse
    {
        $this->execute(new UpdateProduct(
            id: $id,
            name: $this->getRequest()->get('name'),
            uniqueCode: $this->getRequest()->get('uniqueCode'),
            size: $this->getRequest()->get('size')
        )
        );

        return $this->json(null, 204);
    }

    /**
     * @OA\GET(summary="Получение одного продукта по id")
     *
     * @OA\Response(
     *     response=200
     *     description="Список всех продуктов"
     * )
     *
     * @throws \Exception
     */
    #[Route('/products/{id}', name: 'app_product.getProduct', methods: ['GET'])]
    public function getProduct($id): JsonResponse
    {
        $result = $this->query(new GetProduct(id: $id));

        return $this->json($result);
    }
}
