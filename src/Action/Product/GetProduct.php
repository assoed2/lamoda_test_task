<?php

namespace App\Action\Product;

use App\Common\Query;

final readonly class GetProduct implements Query
{
    public function __construct(public int $id
    ) {
    }
}
