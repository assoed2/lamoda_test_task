<?php

namespace App\Action\Product;

use App\Common\Command;

final readonly class UpdateProduct implements Command
{
    public function __construct(
        public string $id,
        public string $name,
        public string $uniqueCode,
        public string $size,
    ) {
    }
}
