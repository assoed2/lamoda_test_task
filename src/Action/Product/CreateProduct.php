<?php

namespace App\Action\Product;

use App\Common\Command;

final readonly class CreateProduct implements Command
{
    public function __construct(
        public string $name,
        public string $uniqueCode,
        public string $size,
    ) {
    }
}
