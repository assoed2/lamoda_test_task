<?php

namespace App\Action\Storage;

use App\Common\Query;

final readonly class GetStorage implements Query
{
    public function __construct(
        public int $id
    ) {
    }
}
