<?php

namespace App\Action\Storage;

use App\Common\Command;

final readonly class CreateStorage implements Command
{
    public function __construct(
        public string $name,
        public string $status,
    ) {
    }
}
