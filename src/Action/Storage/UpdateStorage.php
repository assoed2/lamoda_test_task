<?php

namespace App\Action\Storage;

use App\Common\Command;

final readonly class UpdateStorage implements Command
{
    public function __construct(
        public int $id,
        public string $name,
        public string $status,
    ) {
    }
}
