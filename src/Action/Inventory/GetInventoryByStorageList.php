<?php

namespace App\Action\Inventory;

use App\Common\Query;

final readonly class GetInventoryByStorageList implements Query
{
    public function __construct(public int $id)
    {
    }
}
