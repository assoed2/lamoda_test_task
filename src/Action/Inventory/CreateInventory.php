<?php

namespace App\Action\Inventory;

use App\Common\Command;

final readonly class CreateInventory implements Command
{
    public function __construct(
        public int $productId,
        public int $storageId,
        public int $amount,
    ) {
    }
}
