<?php

namespace App\Action\Inventory;

use App\Common\Command;

final readonly class ReserveInventory implements Command
{
    public function __construct(public array $reserve)
    {
    }
}
