<?php

namespace App\Action\Inventory;

use App\Common\Command;

final readonly class UpdateInventory implements Command
{
    public function __construct(
        public int $inventoryId,
        public int $productId,
        public int $storageId,
        public int $amount,
    ) {
    }
}
