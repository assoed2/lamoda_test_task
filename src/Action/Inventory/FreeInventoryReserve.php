<?php

namespace App\Action\Inventory;

use App\Common\Command;

final readonly class FreeInventoryReserve implements Command
{
    public function __construct(public array $reserve)
    {
    }
}
