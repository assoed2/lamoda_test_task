<?php

namespace App\Entity;

use App\Repository\InventoryRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: InventoryRepository::class)]
class Inventory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $amount = null;

    #[ORM\ManyToOne(targetEntity: Product::class, inversedBy: 'inventories')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Product $product = null;

    #[ORM\ManyToOne(targetEntity: Storage::class, inversedBy: 'inventories')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Storage $storage = null;

    private function __construct(Product $product, Storage $storage, int $amount)
    {
        $this->product = $product;
        $this->storage = $storage;
        $this->amount = $amount;
    }

    public static function create(Product $product, Storage $storage, int $amount): self
    {
        return new self(
            $product,
            $storage,
            $amount
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): static
    {
        $this->amount = $amount;

        return $this;
    }

    public function setProduct(Product $product): static
    {
        $this->product = $product;

        return $this;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setStorage(Storage $storage): static
    {
        $this->storage = $storage;

        return $this;
    }

    public function getStorage(): ?Storage
    {
        return $this->storage;
    }
}
