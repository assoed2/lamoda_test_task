<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 15)]
    private ?string $uniqueCode = null;

    #[ORM\Column(length: 50)]
    private ?string $size = null;

    #[ORM\OneToMany(targetEntity: Inventory::class, mappedBy: 'product')]
    private Collection $inventories;

    public static function create(string $name, string $uniqueCode, string $size): self
    {
        return new self(
            $name,
            $uniqueCode,
            $size
        );
    }

    public function __construct(string $name, string $uniqueCode, string $size)
    {
        $this->name = $name;
        $this->uniqueCode = $uniqueCode;
        $this->size = $size;
        $this->inventories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $Name): static
    {
        $this->name = $Name;

        return $this;
    }

    public function getUniqueCode(): ?string
    {
        return $this->uniqueCode;
    }

    public function setUniqueCode(?string $uniqueCode): static
    {
        $this->uniqueCode = $uniqueCode;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(?string $size): static
    {
        $this->size = $size;

        return $this;
    }

    public function getInventories(): Collection
    {
        return $this->inventories;
    }

    public function setInventories(Collection $Inventories): static
    {
        $this->inventories = $Inventories;

        return $this;
    }
}
