<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Product::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Product $product;

    #[ORM\ManyToOne(targetEntity: Storage::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Storage $storage;

    #[ORM\Column(type: 'integer')]
    private int $amount;

    #[ORM\Column(type: 'datetime')]
    private \DateTimeInterface $reservedAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $releasedAt = null;

    public function __construct(Product $product, Storage $storage, int $amount)
    {
        $this->product = $product;
        $this->storage = $storage;
        $this->amount = $amount;
        $this->reservedAt = new \DateTimeImmutable();
    }

    public static function create(Product $product, Storage $storage, int $amount): self
    {
        return new self(
            $product,
            $storage,
            $amount
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getStorage(): Storage
    {
        return $this->storage;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): void
    {
        $this->amount = $amount;
    }

    public function getReservedAt(): \DateTimeInterface
    {
        return $this->reservedAt;
    }

    public function getReleasedAt(): ?\DateTimeInterface
    {
        return $this->releasedAt;
    }

    public function setReleasedAt(?\DateTimeInterface $releasedAt): static
    {
        $this->releasedAt = $releasedAt;

        return $this;
    }
}
