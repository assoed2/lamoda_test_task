<?php

namespace App\ActionHandler\Storage;

use App\Action\Storage\GetStorage;
use App\Common\QueryHandler;
use App\DTO\storageDTO;
use App\Entity\Storage;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class GetStorageHandler implements QueryHandler
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function __invoke(GetStorage $query): ArrayCollection
    {
        $this->verify();

        return $this->handle($query);
    }

    private function verify()
    {
    }

    private function handle(GetStorage $query): ArrayCollection
    {
        $storage = $this->entityManager->getRepository(Storage::class)->find($query->id);

        $storageDTO = new ArrayCollection();

        $storageDTO->add(new storageDTO(
            id: $storage->getId(),
            name: $storage->getName(),
            status: $storage->getStatus(),
        ));

        return $storageDTO;
    }
}
