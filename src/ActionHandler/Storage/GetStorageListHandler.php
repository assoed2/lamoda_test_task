<?php

namespace App\ActionHandler\Storage;

use App\Action\Storage\GetStorageList;
use App\Common\QueryHandler;
use App\DTO\StorageDTO;
use App\Entity\Storage;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class GetStorageListHandler implements QueryHandler
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function __invoke(GetStorageList $query): ArrayCollection
    {
        $this->verify();

        return $this->handle($query);
    }

    private function verify()
    {
    }

    private function handle(GetStorageList $query): ArrayCollection
    {
        $storages = $this->entityManager->getRepository(Storage::class)->findAll();

        $storageDTOs = new ArrayCollection();
        foreach ($storages as $storage) {
            $storageDTOs->add(new StorageDTO(
                id: $storage->getId(),
                name: $storage->getName(),
                status: $storage->getStatus(),
            ));
        }

        return $storageDTOs;
    }
}
