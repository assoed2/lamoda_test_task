<?php

namespace App\ActionHandler\Storage;

use App\Action\Storage\UpdateStorage;
use App\Common\CommandHandler;
use App\Entity\Storage;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

class UpdateStorageHandler implements CommandHandler
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function __invoke(UpdateStorage $command): void
    {
        $this->verify($command);

        $this->handle($command);
    }

    private function handle(UpdateStorage $command): void
    {
        $storage = $this->entityManager->getRepository(Storage::class)->find($command->id);

        if (null !== $command->name) {
            $storage->setName($command->name);
        }

        if (null !== $command->name) {
            $storage->setStatus($command->status);
        }

        try {
            $this->entityManager->beginTransaction();
            $this->entityManager->lock($storage, LockMode::PESSIMISTIC_READ);
            $this->entityManager->persist($storage);
            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (\Exception $exception) {
            $this->entityManager->rollback();
        }
    }

    private function verify(UpdateStorage $command): void
    {
        $storage = $this->entityManager->getRepository(Storage::class)->find($command->id);

        if (null === $storage) {
            throw new EntityNotFoundException('Product not found');
        }

        if (strlen($command->name) > 60) {
            throw new \Exception("Storage name can't be more than 60 characters");
        }

        if (strlen($command->status) > 20) {
            throw new \Exception("Storage size can't be more than 20 characters");
        }
    }
}
