<?php

declare(strict_types=1);

namespace App\ActionHandler\Storage;

use App\Action\Storage\CreateStorage;
use App\Common\CommandHandler;
use App\Entity\Storage;
use Doctrine\ORM\EntityManagerInterface;

class CreateStorageHandler implements CommandHandler
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function __invoke(CreateStorage $command): void
    {
        $this->verify($command);

        $this->handle($command);
    }

    private function handle(CreateStorage $command): void
    {
        $storage = Storage::create(
            $command->name,
            $command->status
        );

        $this->entityManager->persist($storage);
        $this->entityManager->flush();
    }

    private function verify(CreateStorage $command): void
    {
        if (strlen($command->name) > 60) {
            throw new \Exception("Storage name can't be more than 60 characters");
        }

        if (strlen($command->status) > 20) {
            throw new \Exception("Storage size can't be more than 20 characters");
        }
    }
}
