<?php

namespace App\ActionHandler\Product;

use App\Action\Product\GetProductsList;
use App\Common\QueryHandler;
use App\DTO\ProductDTO;
use App\Entity\Product;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class GetProductsListHandler implements QueryHandler
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function __invoke(GetProductsList $query): ArrayCollection
    {
        $this->verify();

        return $this->handle($query);
    }

    private function verify()
    {
    }

    private function handle(GetProductsList $query): ArrayCollection
    {
        $products = $this->entityManager->getRepository(Product::class)->findAll();

        $productDTOs = new ArrayCollection();
        foreach ($products as $product) {
            $productDTOs->add(new ProductDTO(
                id: $product->getId(),
                name: $product->getName(),
                uniqueCode: $product->getUniqueCode(),
                size: $product->getSize()
            ));
        }

        return $productDTOs;
    }
}
