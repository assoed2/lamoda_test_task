<?php

namespace App\ActionHandler\Product;

use App\Action\Product\GetProduct;
use App\Common\QueryHandler;
use App\DTO\ProductDTO;
use App\Entity\Product;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class GetProductHandler implements QueryHandler
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function __invoke(GetProduct $query): ArrayCollection
    {
        $this->verify();

        return $this->handle($query);
    }

    private function verify()
    {
    }

    private function handle(GetProduct $query): ArrayCollection
    {
        $product = $this->entityManager->getRepository(Product::class)->find($query->id);

        $productDTO = new ArrayCollection();

        $productDTO->add(new ProductDTO(
            id: $product->getId(),
            name: $product->getName(),
            uniqueCode: $product->getUniqueCode(),
            size: $product->getSize()
        ));

        return $productDTO;
    }
}
