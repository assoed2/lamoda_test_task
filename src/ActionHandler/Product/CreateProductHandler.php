<?php

declare(strict_types=1);

namespace App\ActionHandler\Product;

use App\Action\Product\CreateProduct;
use App\Common\CommandHandler;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;

class CreateProductHandler implements CommandHandler
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function __invoke(CreateProduct $command): void
    {
        $this->verify($command);

        $this->handle($command);
    }

    private function handle(CreateProduct $command): void
    {
        $product = Product::create(
            $command->name,
            $command->uniqueCode,
            $command->size
        );

        $this->entityManager->persist($product);
        $this->entityManager->flush();
    }

    private function verify(CreateProduct $command): void
    {
        if (null === $command->name || '' === $command->name) {
            throw new \Exception("Product name can't be empty");
        }

        if (null === $command->size || '' === $command->size) {
            throw new \Exception("Product size can't be empty");
        }

        if (null === $command->uniqueCode || '' === $command->uniqueCode) {
            throw new \Exception("Product unique code can't be empty");
        }

        if (strlen($command->name) > 255) {
            throw new \Exception("Product name can't be more than 255 characters");
        }

        if (strlen($command->size) > 50) {
            throw new \Exception("Product size can't be more than 50 characters");
        }

        if (strlen($command->uniqueCode) > 15) {
            throw new \Exception("Product unique code can't be more than 15 characters");
        }
    }
}
