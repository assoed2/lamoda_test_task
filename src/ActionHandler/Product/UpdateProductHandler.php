<?php

namespace App\ActionHandler\Product;

use App\Action\Product\UpdateProduct;
use App\Common\CommandHandler;
use App\Entity\Product;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

class UpdateProductHandler implements CommandHandler
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function __invoke(UpdateProduct $command): void
    {
        $this->verify($command);

        $this->handle($command);
    }

    private function handle(UpdateProduct $command): void
    {
        $product = $this->entityManager->getRepository(Product::class)->find($command->id);
        $product->setName($command->name);
        $product->setSize($command->size);
        $product->setUniqueCode($command->uniqueCode);
        try {
            $this->entityManager->beginTransaction();
            $this->entityManager->lock($product, LockMode::PESSIMISTIC_READ);
            $this->entityManager->persist($product);
            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (\Exception $exception) {
            $this->entityManager->rollback();
        }
    }

    private function verify(UpdateProduct $command): void
    {
        $product = $this->entityManager->getRepository(Product::class)->find($command->id);

        if (null === $product) {
            throw new EntityNotFoundException('Product not found');
        }

        if (null === $command->name || '' === $command->name) {
            throw new \Exception("Product name can't be empty");
        }

        if (null === $command->size || '' === $command->size) {
            throw new \Exception("Product size can't be empty");
        }

        if (null === $command->uniqueCode || '' === $command->uniqueCode) {
            throw new \Exception("Product unique code can't be empty");
        }

        if (strlen($command->name) > 255) {
            throw new \Exception("Product name can't be more than 255 characters");
        }

        if (strlen($command->size) > 50) {
            throw new \Exception("Product size can't be more than 50 characters");
        }

        if (strlen($command->uniqueCode) > 15) {
            throw new \Exception("Product unique code can't be more than 15 characters");
        }
    }
}
