<?php

namespace App\ActionHandler\Inventory;

use App\Action\Inventory\FreeInventoryReserve;
use App\Common\CommandHandler;
use App\Entity\Inventory;
use App\Entity\Product;
use App\Entity\Reservation;
use App\Entity\Storage;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

class FreeInventoryReserveHandler implements CommandHandler
{
    public function __invoke(FreeInventoryReserve $command): void
    {
        $this->handle($command);
    }

    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    private function handle(FreeInventoryReserve $command): void
    {
        $reservations = $command->reserve;

        foreach ($reservations as $reservationData) {
            $product = $this->entityManager->getRepository(Product::class)->findOneBy(['uniqueCode' => $reservationData['uniqueCode']]);
            $storage = $this->entityManager->getRepository(Storage::class)->find($reservationData['storageId']);
            $reservation = $this->entityManager->getRepository(Reservation::class)->findOneBy(['product' => $product, 'storage' => $storage]);
            $inventory = $this->entityManager->getRepository(Inventory::class)->findOneBy(['product' => $product, 'storage' => $storage]);
            $amount = $reservation->getAmount();

            if (null === $product) {
                throw new EntityNotFoundException('Product not found');
            }

            if (null === $storage) {
                throw new EntityNotFoundException('Storage not found');
            }

            if ($amount > $inventory->getAmount()) {
                throw new \Exception('Not enough inventory to reserve');
            }

            $inventory->setAmount($inventory->getAmount() + $amount);
            $reservation->setReleasedAt(new \DateTime());
            try {
                $this->entityManager->beginTransaction();
                $this->entityManager->lock($inventory, LockMode::PESSIMISTIC_READ);
                $this->entityManager->lock($reservation, LockMode::PESSIMISTIC_READ);
                $this->entityManager->persist($inventory);
                $this->entityManager->persist($reservation);
                $this->entityManager->flush();
                $this->entityManager->commit();
            } catch (\Exception $exception) {
                $this->entityManager->rollback();
            }
        }
    }
}
