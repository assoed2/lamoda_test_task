<?php

namespace App\ActionHandler\Inventory;

use App\Action\Inventory\UpdateInventory;
use App\Common\Command;
use App\Entity\Inventory;
use App\Entity\Product;
use App\Entity\Storage;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

class UpdateInventoryHandler implements Command
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function __invoke(UpdateInventory $command): void
    {
        $this->verify($command);

        $this->handle($command);
    }

    private function handle(UpdateInventory $command): void
    {
        // TODO: реализовать возможность обновлять только одно поле

        $inventory = $this->entityManager->getRepository(Inventory::class)->find($command->inventoryId);

        if (null === $inventory) {
            throw new EntityNotFoundException('Inventory not found');
        }

        $product = $this->entityManager->getRepository(Product::class)->find($command->productId);

        if (null === $product) {
            throw new EntityNotFoundException('Product not found');
        }

        $storage = $this->entityManager->getRepository(Storage::class)->find($command->storageId);

        if (null === $storage) {
            throw new EntityNotFoundException('Storage not found');
        }

        $inventory->setProduct($product);
        $inventory->setStorage($storage);

        if (null !== $command->amount) {
            $inventory->setAmount($command->amount);
        }
        try {
            $this->entityManager->beginTransaction();
            $this->entityManager->lock($storage, LockMode::PESSIMISTIC_READ);
            $this->entityManager->persist($storage);
            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (\Exception $exception) {
            $this->entityManager->rollback();
        }
    }

    private function verify(UpdateInventory $command): void
    {
    }
}
