<?php

namespace App\ActionHandler\Inventory;

use App\Action\Inventory\ReserveInventory;
use App\Common\CommandHandler;
use App\Entity\Inventory;
use App\Entity\Product;
use App\Entity\Reservation;
use App\Entity\Storage;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

class ReserveInventoryHandler implements CommandHandler
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function __invoke(ReserveInventory $command): void
    {
        $this->verify();
        $this->handle($command);
    }

    private function verify(): void
    {
    }

    private function handle(ReserveInventory $command): void
    {
        $reservations = $command->reserve;

        foreach ($reservations as $reservation) {
            $product = $this->entityManager->getRepository(Product::class)->findOneBy(['uniqueCode' => $reservation['uniqueCode']]);
            $storage = $this->entityManager->getRepository(Storage::class)->find($reservation['storageId']);
            $inventory = $this->entityManager->getRepository(Inventory::class)->findOneBy(['product' => $product, 'storage' => $storage]);
            $amount = $reservation['amount'];

            if (null === $product) {
                throw new EntityNotFoundException('Product not found');
            }

            if (null === $storage) {
                throw new EntityNotFoundException('Storage not found');
            }

            if ($amount > $inventory->getAmount()) {
                throw new \Exception('Not enough inventory to reserve');
            }

            $reservationData = Reservation::create(
                $product,
                $storage,
                $amount
            );

            $inventory->setAmount($inventory->getAmount() - $amount);

            try {
                $this->entityManager->beginTransaction();
                $this->entityManager->lock($inventory, LockMode::PESSIMISTIC_READ);
                $this->entityManager->persist($inventory);
                $this->entityManager->persist($reservationData);
                $this->entityManager->flush();
                $this->entityManager->commit();
            } catch (\Exception $exception) {
                $this->entityManager->rollback();
            }
        }
    }
}
