<?php

namespace App\ActionHandler\Inventory;

use App\Action\Inventory\GetInventoryByStorageList;
use App\Common\QueryHandler;
use App\DTO\InventoryDTO;
use App\DTO\ProductDTO;
use App\DTO\StorageDTO;
use App\Entity\Inventory;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class GetInventoryByStorageListHandler implements QueryHandler
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function __invoke(GetInventoryByStorageList $query): ArrayCollection
    {
        $this->verify();

        return $this->handle($query);
    }

    private function verify()
    {
    }

    private function handle(GetInventoryByStorageList $query): ArrayCollection
    {
        $inventories = $this->entityManager->getRepository(Inventory::class)->findBy(['storage' => $query->id]);

        $inventoryDTOs = new ArrayCollection();
        foreach ($inventories as $inventory) {
            $storageDTO = StorageDTO::fromObject($inventory->getStorage());
            $productDTO = ProductDTO::fromObject($inventory->getProduct());

            $inventoryDTOs->add(new InventoryDTO(
                id: $inventory->getId(),
                product: $productDTO,
                storage: $storageDTO,
                amount: $inventory->getAmount(),
            ));
        }

        return $inventoryDTOs;
    }
}
