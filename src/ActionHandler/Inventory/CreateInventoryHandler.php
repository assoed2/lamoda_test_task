<?php

namespace App\ActionHandler\Inventory;

use App\Action\Inventory\CreateInventory;
use App\Common\CommandHandler;
use App\Entity\Inventory;
use App\Entity\Product;
use App\Entity\Storage;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

class CreateInventoryHandler implements CommandHandler
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function __invoke(CreateInventory $command): void
    {
        $this->verify($command);
        $this->handle($command);
    }

    public function handle(CreateInventory $command): void
    {
        $product = $this->entityManager->getRepository(Product::class)->find($command->productId);

        // TODO: если товар уже есть на складе не записывать, а обновлять

        if (null === $product) {
            throw new EntityNotFoundException('Product not found');
        }

        $storage = $this->entityManager->getRepository(Storage::class)->find($command->storageId);

        if (null === $storage) {
            throw new EntityNotFoundException('Storage not found');
        }

        $inventory = Inventory::create(
            $product,
            $storage,
            $command->amount,
        );

        $this->entityManager->persist($inventory);
        $this->entityManager->flush();
    }

    public function verify(CreateInventory $command): void
    {
        if ($command->amount < 1) {
            throw new \InvalidArgumentException('Amount must be greater than 0');
        }
    }
}
