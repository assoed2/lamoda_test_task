FROM php:8.3-fpm-alpine

# Обновление пакетов и установка необходимых зависимостей, включая curl
RUN apk update && apk upgrade && \
    apk add --no-cache icu-dev postgresql-dev libzip-dev g++ make curl && \
    docker-php-ext-configure intl && \
    docker-php-ext-install pdo_pgsql pgsql zip intl && \
    rm -rf /var/cache/apk/* && rm -rf /tmp/*

# Установка и включение расширения Redis
RUN apk add --no-cache pcre-dev $PHPIZE_DEPS && \
    pecl install redis && docker-php-ext-enable redis

# Установка заголовков для Linux
RUN apk add --update linux-headers

# Установка и настройка Xdebug
RUN pecl install xdebug-3.3.2 \
    && docker-php-ext-enable xdebug \
    && echo "xdebug.mode=debug" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.client_host=host.docker.internal" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

# Использование php.ini для разработки
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

# Установка Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Установка рабочей директории
WORKDIR /app

RUN mkdir -p var/log \
    && chmod -R 777 var/log